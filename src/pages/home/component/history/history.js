import React from 'react';

import './history.css'

class Info extends React.Component {
	render() {
		return(
			<div className="infobox">
				<p>{this.props.cityname}</p>
				<p>{this.props.temperature}</p>
			</div>
		);
	}
}

export default Info;