import React from 'react';
import History from '../history';

import './form.css';

const API_KEY = "0513b1d466a65e9f95d7ea62563355f2";

class Form extends React.Component {
	
	constructor(props) {
		super(props);
		this.state = {
			selected_option: '',
			search_text: '',
			weather_info: 'No result !',
			showError: false,
			error: '',
			history: []
		}
	}

	componentDidMount = () => {
		console.log(this.state.history,'------------->history')
        //check for localStorage every time component mounts. You can also do this conditionally.
        this.recover();
    }

	onSelectOptionChange = (event) => {
		this.setState({
	   		selected_option: event.target.value,
	   		showError: (event.target.value ==='') ? true : false,
	   		error: (event.target.value ==='') ? 'Please select an option' : ''
	 	});
		
	}

	onSearchBoxChange = async (event) => {
		await this.setState({
	   		search_text: event.target.value
	 	});

	 	this.getWeather(event);
	}

	recover = () =>{
        //parse the localstorage value
        let data = localStorage.getItem('history') ? JSON.parse(localStorage.getItem('history')) :  [];
        console.log(data,'--------->local')
        this.setState({history: data});
    }

	getWeather = async (e) => {
		const selected_option = this.state.selected_option;
		const search_text = this.state.search_text;
    	if(selected_option !== '' && search_text !== ''){
	    	let api_url = '';
			if(selected_option === 'city'){
				api_url = `http://api.openweathermap.org/data/2.5/weather?q=${search_text}&appid=${API_KEY}&units=metric`;
			} else if(selected_option === 'zip_code') {
				api_url = `http://api.openweathermap.org/data/2.5/weather?zip=${search_text},IN&appid=${API_KEY}&units=metric`;
			}else if(selected_option === 'lat_long') {
				let splited_search_text = search_text.split(",");
				const lat = splited_search_text[0].trim();
				const long = splited_search_text[1] ? splited_search_text[1].trim() : '0';
				
				api_url = `http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${long}&appid=${API_KEY}&units=metric`;
			}
			const api_call = await fetch(api_url);
			const data = await api_call.json();
			if(data.cod !== '404' && data.cod !== '400'){
				
				//Save search history in local storage(Only last 3 search history)
				console.log(this.state.history,'------------->history inside getWeather')
				let history = this.state.history ? this.state.history : [];
				if(history.length === 3) {
					history.splice(0,1)
				}
		        history.push({temperature:data.main.temp+' °C', city:search_text});
		        localStorage.setItem('history', JSON.stringify(history));

				this.setState({
			        weather_info: `${data.main.temp} °C ${data.weather[0].description}`,
			        showError: false,
		        	error: ''
		      	});
			}else{
				this.setState({
			        weather_info: 'No reasult !',
			    	showError: false,
		        	error: ''
		      	});
			}
		}else{
			this.setState({
		        weather_info: 'No result !',
		        showError: true,
		        error: (selected_option === '' && search_text === '') ? 'Please select an option and search something.' : ((selected_option ==='') ? 'Please select an option' : 'Please search something')
	      	});
		}
	}

	render() {
		return (
			<>
				<div className="search-container">
					{this.state.showError && <div className="error-message">{this.state.error}</div>}
					<div className="rox">
					    <div className="col-md-8">
					    	<input type="text" className="form-control" name="searchbox" onChange={(e) => this.onSearchBoxChange(e)}  placeholder="Search....."/>
					    </div>
					    <div className="col-md-4">
						    <select name="select_option" className="form-control" onChange={(e) => this.onSelectOptionChange(e)}>
							    <option value="">Select Option</option>
							    <option value="city">City Name</option>
							    <option value="zip_code">Zip Code</option>
							    <option value="lat_long">Lat/Long</option>
						  	</select>
						</div>
					</div>
				</div>
				<div className="weather-container">
					<p>Weather Info : </p><p>{this.state.weather_info}</p>
				</div>

				<div id="main">
					<div className="row">
						{this.state.history ? this.state.history.map((item, i) => {
							return (
	                            //render as state changes
								<div className="col-md-4" key={i}>
									<History cityname={item.city} temperature={item.temperature} />
								</div>	                            
	                        )
	                    }) : 'no history'}	
					</div>
			    </div>
			</>
		)
	}
}

export default Form;